/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.DAO;

import gribanov.sksmaven.enumClass.City;
import gribanov.sksmaven.source.SKSuser;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author artem
 */
public class UserDaoTest {
    
    public UserDaoTest() {
    }

    /**
     * Test of addUser method, of class UserDao.
     */
    @Test
    public void testAddUser() {
        System.out.println("addUser");
        SKSuser user1 = SKSuser.crateNewUser(0, "artgri45@mail.ru", "qweqwe", "Artem", "Gribanov", City.BRNO);
        UserDao instance = new UserDao();
        instance.addUser(user1);
        SKSuser user2 = instance.getUserByEmail("artgri45@mail.ru");
        user2.setId(0);
        assertSame(user2, user1);
    }


    /**
     * Test of getUserByEmail method, of class UserDao.
     */
    @Test
    public void testGetUserByEmail() {
        System.out.println("getUserByEmail");
        UserDao instance = new UserDao();
        String mail = "artgri45@mail.ru";
        SKSuser result = instance.getUserByEmail(mail);
        SKSuser expResult = null;
        assertEquals(expResult, result);
    }
    
}
