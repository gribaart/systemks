/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.form;

import gribanov.sksklient.Klient;
import static gribanov.sksklient.Klient.input;
import gribanov.sksklient.User;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a form for enter end registration.
 * @author artem
 */
public class CompliteForm {

    private static Logger log = Logger.getLogger(CompliteForm.class.getName());

    JLabel titleLabel, nameLabel, surenameLabel, emailLabel, passwordLabel,
            cityLabel, universityLabel, titleLabel2, emailLabel2, passwordLabel2;
    JTextField nameField, surenameField, emailField, emailField2;
    JPasswordField passwordField, passwordField2;
    JButton registerButton, loginButton, registerButton2, loginButton2;
    JComboBox cityComboBox;
    JPanel jpanelReg, jpanelLogin;
    JFrame jframe;
    String city[] = new String[6];
    String nameValue, surenameValue, emailValue, passwordValue, cityValue,
            emailValue2, passwordValue2;
    Klient klient;

    public static void main(String[] args) {
        CompliteForm form = new CompliteForm();
    }

    /**
     *Default Constructor.
     * Creates a new form.
     */
    public CompliteForm() {
        initComponent();
        initEvent();
        klient = new Klient();
        klient.run();
    }

    private void initComponent() {
        //Init start window
        jframe = new JFrame();
        jframe.setTitle("System komunikace studentu");

        jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        jframe.setSize(470, 220);
        jframe.setLocationRelativeTo(null);

        //Init registration page
        jpanelReg = new JPanel();
        jpanelReg.setLayout(null);

        // Defining name of field 
        titleLabel = new JLabel("Registration Form");
        titleLabel.setBounds(150, 10, 200, 30);

        nameLabel = new JLabel("Name");
        nameLabel.setBounds(60, 60, 100, 30);

        surenameLabel = new JLabel("Surname");
        surenameLabel.setBounds(60, 110, 100, 30);

        emailLabel = new JLabel("Email");
        emailLabel.setBounds(60, 160, 100, 30);

        passwordLabel = new JLabel("Password");
        passwordLabel.setBounds(60, 210, 100, 30);

        cityLabel = new JLabel("City");
        cityLabel.setBounds(60, 260, 100, 30);

        // Defining field 
        nameField = new JTextField();
        nameField.setBounds(170, 60, 200, 30);

        surenameField = new JTextField();
        surenameField.setBounds(170, 110, 200, 30);

        emailField = new JTextField();
        emailField.setBounds(170, 160, 200, 30);

        passwordField = new JPasswordField();
        passwordField.setBounds(170, 210, 200, 30);

        cityComboBox = new JComboBox();
        cityComboBox.setBounds(170, 260, 200, 30);
        city[0] = "PRAHA";
        city[1] = "BRNO";
        city[2] = "OSTRAVA";
        city[3] = "PLZEN";
        city[4] = "LIBIREC";
        city[5] = "OLOMOUC";
        for (int j = 0; j < 6; j++) {
            cityComboBox.addItem(city[j]);
        }

        registerButton = new JButton("Registration");
        registerButton.setBounds(60, 310, 150, 30);

        loginButton = new JButton("Log in");
        loginButton.setBounds(220, 310, 150, 30);

        //Init login page//
        jpanelLogin = new JPanel();
        jpanelLogin.setLayout(null);
        jframe.add(jpanelLogin);

        // Defining label
        titleLabel2 = new JLabel("Log in");
        titleLabel2.setBounds(200, 10, 200, 30);

        emailLabel2 = new JLabel("Email");
        emailLabel2.setBounds(60, 60, 100, 30);

        passwordLabel2 = new JLabel("Password");
        passwordLabel2.setBounds(60, 110, 100, 30);

        // Defining field 
        emailField2 = new JTextField();
        emailField2.setBounds(170, 60, 200, 30);

        passwordField2 = new JPasswordField();
        passwordField2.setBounds(170, 110, 200, 30);

        registerButton2 = new JButton("Registration");
        registerButton2.setBounds(60, 160, 150, 30);

        loginButton2 = new JButton("Log in");
        loginButton2.setBounds(220, 160, 150, 30);

        // fixing all Label,TextField,Button 
        jpanelLogin.add(titleLabel2);
        jpanelLogin.add(emailLabel2);
        jpanelLogin.add(passwordLabel2);
        jpanelLogin.add(emailField2);
        jpanelLogin.add(passwordField2);
        jpanelLogin.add(registerButton2);
        jpanelLogin.add(loginButton2);
        jpanelReg.add(titleLabel);
        jpanelReg.add(nameLabel);
        jpanelReg.add(surenameLabel);
        jpanelReg.add(emailLabel);
        jpanelReg.add(passwordLabel);
        jpanelReg.add(cityLabel);
        jpanelReg.add(nameField);
        jpanelReg.add(emailField);
        jpanelReg.add(surenameField);
        jpanelReg.add(passwordField);
        jpanelReg.add(cityComboBox);
        jpanelReg.add(registerButton);
        jpanelReg.add(loginButton);

        jframe.setVisible(true);
    }

    private void initEvent() {
        nameField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                String sag = nameField.getText();
                if (!(((c >= 'a') || (c >= 'A')) && ((c <= 'z') || (c <= 'Z')) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
                    e.consume();
                }
            }
        });
        surenameField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                String sag = nameField.getText();
                if (!(((c >= 'a') || (c >= 'A')) && ((c <= 'z')
                        || (c <= 'Z')) || (c == KeyEvent.VK_BACK_SPACE)
                        || (c == KeyEvent.VK_DELETE))) {
                    e.consume();
                }
            }
        });
        emailField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!((c >= '0') && (c <= '9')
                        || ((c >= 'a') || (c >= 'A')) && ((c <= 'z') || (c <= 'Z'))
                        || (c == '@') || (c == '.')
                        || (c == KeyEvent.VK_BACK_SPACE)
                        || (c == KeyEvent.VK_DELETE))) {
                    // getToolkit().beep();
                    e.consume();
                }
            }
        });
        emailField2.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!((c >= '0') && (c <= '9')
                        || ((c >= 'a') || (c >= 'A')) && ((c <= 'z') || (c <= 'Z'))
                        || (c == '@') || (c == '.')
                        || (c == KeyEvent.VK_BACK_SPACE)
                        || (c == KeyEvent.VK_DELETE))) {
                    // getToolkit().beep();
                    e.consume();
                }
            }
        });
        registerButton.addActionListener((ActionEvent event) -> {
            getUserInformationForReg();
            validation1();
            User user = makeUserForRegistration();
            klient.sendDate(user);
            try {
                User us = (User) input.readObject();
                log.log(Level.INFO, "I have some data!!!!");
                nextStepReg(us);
            } catch (IOException ex) {
                Logger.getLogger(CompliteForm.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(CompliteForm.class.getName()).log(Level.SEVERE, null, ex);
            }

        });

        emailField2.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!((c >= '0') && (c <= '9')
                        || ((c >= 'a') || (c >= 'A')) && ((c <= 'z') || (c <= 'Z'))
                        || (c == '@') || (c == '.')
                        || (c == KeyEvent.VK_BACK_SPACE)
                        || (c == KeyEvent.VK_DELETE))) {
                    // getToolkit().beep();
                    e.consume();
                }
            }
        });

        loginButton.addActionListener((ActionEvent event) -> {
            jframe.setSize(430, 220);
            jframe.remove(jpanelReg);
            jframe.add(jpanelLogin);

        });

        registerButton2.addActionListener((ActionEvent event) -> {
            jframe.setSize(430, 380);
            jframe.remove(jpanelLogin);
            jframe.add(jpanelReg);
        });

        loginButton2.addActionListener((ActionEvent event) -> {
            getUserInformationForLogin();
            validation2();
            User user = makeUserForLogin();
            klient.sendDate(user);
            try {
                User us = (User) input.readObject();
                log.log(Level.INFO, "I have some data!!!!");
                nextStepLogin(us);
            } catch (IOException ex) {
                Logger.getLogger(CompliteForm.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(CompliteForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

    }

    private Boolean validation1() {
        if (nameValue.equals("")) {
            JOptionPane.showMessageDialog(null, "Enter Name");
        } else if (surenameValue.equals("")) {
            JOptionPane.showMessageDialog(null, "Enter Surname");
        } else if (emailValue.equals("")) {
            JOptionPane.showMessageDialog(null, "Enter Email");
        } else if (!(emailValue.contains("@") & emailValue.contains(".")) || emailValue.length() < 5) {
            JOptionPane.showMessageDialog(null, "Enter valid mail");
        } else if (passwordValue.equals("")) {
            JOptionPane.showMessageDialog(null, "Enter Password");
        }

        return true;
    }

    private Boolean validation2() {
        if (emailValue2.equals("")) {
            JOptionPane.showMessageDialog(null, "Enter Email");
        } else if (!(emailValue2.contains("@") & emailValue2.contains(".")) || emailValue2.length() < 5) {
            JOptionPane.showMessageDialog(null, "Enter valid mail");
        } else if (passwordValue2.equals("")) {
            JOptionPane.showMessageDialog(null, "Enter Password");
        }
        return true;
    }

    private void getUserInformationForReg() {
        nameValue = nameField.getText();
        surenameValue = surenameField.getText();
        emailValue = emailField.getText();
        passwordValue = passwordField.getText();
        cityValue = cityComboBox.getSelectedItem().toString();
    }

    private User makeUserForRegistration() {
        int numberOperation = 0;
        User user = new User(numberOperation, nameValue, surenameValue, emailValue,
                passwordValue, cityValue);
        log.log(Level.INFO, user.toString());

        return user;

    }

    private void getUserInformationForLogin() {
        emailValue2 = emailField2.getText();
        passwordValue2 = passwordField2.getText();
    }

    private User makeUserForLogin() {
        int numberOperation = 1;

        User user = new User();
        user.setOperationNumber(numberOperation);
        user.setMail(emailValue2);
        user.setPassword(passwordValue2);
        return user;
    }

    private void nextStepLogin(User user) {
        Boolean reg = user.getLoginComplite();
        log.log(Level.INFO, user.getLoginComplite().toString());
        if (reg) {
            JOptionPane.showMessageDialog(null, "Successfully log in!");
            App app = new App(user, klient);
            jframe.dispose();

        } else {
            JOptionPane.showMessageDialog(null, "Invalide password or email!");
            klient.close();
            klient.run();
            emailField2.setText("");
            passwordField2.setText("");
        }
    }

    private void nextStepReg(User user) {
        Boolean reg = user.getRegistrationCompilte();
        if (reg) {
            JOptionPane.showMessageDialog(null, "Successfully registr!");
            klient.close();
            klient.run();
            jframe.setSize(430, 220);
            jframe.remove(jpanelReg);
            jframe.add(jpanelLogin);
            emailField2.setText(user.getMail());
            passwordField2.setText(user.getPassword());
        } else {
            JOptionPane.showMessageDialog(null, "This email alredy exist!");
            klient.close();
            klient.run();
            emailField.setText("");
            passwordField.setText("");
        }
    }
}
