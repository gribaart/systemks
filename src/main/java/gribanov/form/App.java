/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.form;

import gribanov.sksklient.Klient;
import static gribanov.sksklient.Klient.input;
import gribanov.sksklient.User;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 * Represents a student information system for knowledge exchange.
 * @author artem
 */
public class App {

    private static Logger log = Logger.getLogger(App.class.getName());

    JFrame mainframe;
    JPanel panelProfil, panelNM, panelKE;
    JComboBox cityComboBox3;
    JButton deletBottom, updateBottom;
    JButton profilBottom1, listBottom1, relationBottom1;
    JButton profilBottom2, listBottom2, relationBottom2;
    JButton profilBottom3, listBottom3, relationBottom3;
    JButton delUsBottom, delUnBottom, refreshBottomNM, refreshBottomKE;
    JCheckBox cvut3, czu3, vse3, uk3, ujop3, jipka3;
    JCheckBox lagOf, angOf, maOf, czOf, nemOf, waOf;
    JCheckBox lagDes, angDes, maDes, czDes, nemDes, waDes;
    Boolean cvutValue3, czuValue3, vseValue3, ukValue3, ujopValue3, jipkaValue3;
    JLabel titleLabel3, nameLabel3, surenameLabel3, emailLabel3, passwordLabel3,
            cityLabel3, universityLabel3, subjectLabel3, offerLable3, desireLabel3;
    JTextField nameField3, surenameField3, emailField3;
    JTextField idUnField, idUsField;
    JTable tabelNM;
    JTable tabelKE;
    JScrollPane scrollPanelNM;
    JScrollPane scrollPanelKE;
    JPasswordField passwordField3;
    String city2[] = new String[6];
    Klient klient;
    String city3[] = new String[6];
    String nameValue, surenameValue, emailValue, passwordValue, cityValue;
    Boolean cvutValue, czuValue, vseValue, ukValue, ujopValue, jipkaValue;
    Boolean lagOfValue, angOfValue, maOfValue, czOfValue, nemOfValue, waOfValue;
    Boolean lagDesValue, angDesValue, maDesValue, czDesValue, nemDesValue, waDesValue;
    User oldUser;
    Object[] headersNM = {"IdUn", "University", "Student", "IdUs"};
    Object[] headersKE = {"Name", "Surname", "Mail", "City", "Subject"};
    Object[][] dataNM = {{"please", "refresh", "the", "page"}};
    Object[][] dataKE = {{"please", "press", "the", "refresh","button"}};

    /**
     * Default Constructor.
     * Creates a application window.
     */
    public App(User oldUser, Klient klient) {
        this.oldUser = oldUser;
        this.klient = klient;
        initComponent();
        initEvent();
        getOldUserInformation();

    }

    private void initComponent() {

        mainframe = new JFrame();
        mainframe.setTitle("System komunikace studentu");
        mainframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        mainframe.setSize(700, 460);
        mainframe.setLocationRelativeTo(null);

        //jpanel edit profile
        panelProfil = new JPanel();
        panelProfil.setLayout(null);

        panelNM = new JPanel();
        panelNM.setLayout(null);

        panelKE = new JPanel();
        panelKE.setLayout(null);


        mainframe.add(panelProfil);

        titleLabel3 = new JLabel("Profil information");
        titleLabel3.setBounds(170, 20, 200, 30);

        nameLabel3 = new JLabel("Name");
        nameLabel3.setBounds(60, 60, 100, 30);

        surenameLabel3 = new JLabel("Surname");
        surenameLabel3.setBounds(60, 110, 100, 30);

        emailLabel3 = new JLabel("Email");
        emailLabel3.setBounds(60, 160, 100, 30);

        passwordLabel3 = new JLabel("Password");
        passwordLabel3.setBounds(60, 210, 100, 30);

        cityLabel3 = new JLabel("City");
        cityLabel3.setBounds(60, 260, 100, 30);

        universityLabel3 = new JLabel("University");
        universityLabel3.setBounds(420, 20, 100, 30);

        nameField3 = new JTextField();
        nameField3.setBounds(170, 60, 200, 30);

        surenameField3 = new JTextField();
        surenameField3.setBounds(170, 110, 200, 30);

        emailField3 = new JTextField();
        emailField3.setBounds(170, 160, 200, 30);

        passwordField3 = new JPasswordField();
        passwordField3.setBounds(170, 210, 200, 30);

        offerLable3 = new JLabel("Desire knowledge");
        offerLable3.setBounds(420, 160, 150, 30);

        desireLabel3 = new JLabel("Offer knowledge");
        desireLabel3.setBounds(420, 300, 150, 30);

        cityComboBox3 = new JComboBox();
        cityComboBox3.setBounds(170, 260, 200, 30);
        city3[0] = "PRAHA";
        city3[1] = "BRNO";
        city3[2] = "OSTRAVA";
        city3[3] = "PLZEN";
        city3[4] = "LIBIREC";
        city3[5] = "OLOMOUC";
        for (int j = 0; j < 6; j++) {
            cityComboBox3.addItem(city3[j]);
        }

        cvut3 = new JCheckBox("CVUT");
        cvut3.setBounds(420, 56, 65, 20);

        czu3 = new JCheckBox("CZU");
        czu3.setBounds(420, 80, 60, 20);

        vse3 = new JCheckBox("VSE");
        vse3.setBounds(485, 56, 60, 20);

        uk3 = new JCheckBox("UK");
        uk3.setBounds(485, 80, 60, 20);

        ujop3 = new JCheckBox("UJOP");
        ujop3.setBounds(550, 56, 60, 20);

        jipka3 = new JCheckBox("JIPKA");
        jipka3.setBounds(550, 80, 60, 20);

        updateBottom = new JButton("Update");
        updateBottom.setBounds(60, 343, 150, 40);

        deletBottom = new JButton("Delete");
        deletBottom.setBounds(220, 343, 150, 40);

        lagOf = new JCheckBox("LAG");
        lagOf.setBounds(420, 340, 65, 20);

        maOf = new JCheckBox("MA");
        maOf.setBounds(420, 364, 60, 20);

        angOf = new JCheckBox("ANG");
        angOf.setBounds(485, 340, 60, 20);

        nemOf = new JCheckBox("NEM");
        nemOf.setBounds(485, 364, 60, 20);

        czOf = new JCheckBox("CZ");
        czOf.setBounds(550, 340, 60, 20);

        waOf = new JCheckBox("WA");
        waOf.setBounds(550, 364, 60, 20);

        lagDes = new JCheckBox("LAG");
        lagDes.setBounds(420, 200, 65, 20);

        maDes = new JCheckBox("MA");
        maDes.setBounds(420, 224, 60, 20);

        angDes = new JCheckBox("ANG");
        angDes.setBounds(485, 200, 60, 20);

        nemDes = new JCheckBox("NEM");
        nemDes.setBounds(485, 224, 60, 20);

        czDes = new JCheckBox("CZ");
        czDes.setBounds(550, 200, 60, 20);

        waDes = new JCheckBox("WA");
        waDes.setBounds(550, 224, 60, 20);

        listBottom1 = new JButton("Knowledge exchenge");
        listBottom1.setBounds(0, 0, 233, 18);
        relationBottom1 = new JButton("N:M raletionship");
        relationBottom1.setBounds(233, 0, 233, 18);
        profilBottom1 = new JButton("-My profile-");
        profilBottom1.setBounds(466, 0, 234, 18);

        listBottom2 = new JButton("Knowledge exchenge");
        listBottom2.setBounds(0, 0, 233, 18);
        relationBottom2 = new JButton("-N:M raletionship-");
        relationBottom2.setBounds(233, 0, 233, 18);
        profilBottom2 = new JButton("My profile");
        profilBottom2.setBounds(466, 0, 234, 18);

        listBottom3 = new JButton("-Knowledge exchenge-");
        listBottom3.setBounds(0, 0, 233, 18);
        relationBottom3 = new JButton("N:M raletionship");
        relationBottom3.setBounds(233, 0, 233, 18);
        profilBottom3 = new JButton("My profile");
        profilBottom3.setBounds(466, 0, 234, 18);

        idUnField = new JTextField();
        idUnField.setBounds(50, 40, 20, 20);
        idUsField = new JTextField();
        idUsField.setBounds(640, 40, 20, 20);
        delUnBottom = new JButton("Delete university");
        delUnBottom.setBounds(90, 40, 250, 20);
        delUsBottom = new JButton("Delete student");
        delUsBottom.setBounds(370, 40, 250, 20);
        refreshBottomNM = new JButton("Refresh");
        refreshBottomNM.setBounds(100, 400, 500, 40);

        refreshBottomKE = new JButton("Refresh");
        refreshBottomKE.setBounds(100, 400, 500, 40);

        tabelNM = new JTable(dataNM, headersNM);
        tabelKE = new JTable(dataKE, headersKE);
        scrollPanelNM = new JScrollPane(tabelNM);
        scrollPanelNM.setBounds(100, 70, 500, 300);

        scrollPanelKE = new JScrollPane(tabelKE);
        scrollPanelKE.setBounds(50, 70, 600, 300);

        panelNM.add(scrollPanelNM);
        panelKE.add(scrollPanelKE);
        panelKE.add(refreshBottomKE);

        panelNM.add(idUnField);
        panelNM.add(idUsField);
        panelNM.add(delUnBottom);
        panelNM.add(delUsBottom);
        panelNM.add(refreshBottomNM);

        panelProfil.add(listBottom1);
        panelProfil.add(relationBottom1);
        panelProfil.add(profilBottom1);

        panelKE.add(listBottom3);
        panelKE.add(relationBottom3);
        panelKE.add(profilBottom3);

        panelNM.add(listBottom2);
        panelNM.add(relationBottom2);
        panelNM.add(profilBottom2);

        panelProfil.add(titleLabel3);
        panelProfil.add(nameLabel3);
        panelProfil.add(surenameLabel3);
        panelProfil.add(emailLabel3);
        panelProfil.add(passwordLabel3);
        panelProfil.add(cityLabel3);
        panelProfil.add(universityLabel3);
        panelProfil.add(nameField3);
        panelProfil.add(emailField3);
        panelProfil.add(surenameField3);
        panelProfil.add(passwordField3);
        panelProfil.add(cityComboBox3);
        panelProfil.add(cvut3);
        panelProfil.add(czu3);
        panelProfil.add(vse3);
        panelProfil.add(uk3);
        panelProfil.add(ujop3);
        panelProfil.add(jipka3);
        panelProfil.add(updateBottom);
        panelProfil.add(deletBottom);
        panelProfil.add(offerLable3);
        panelProfil.add(desireLabel3);
        panelProfil.add(lagOf);
        panelProfil.add(waOf);
        panelProfil.add(nemOf);
        panelProfil.add(czOf);
        panelProfil.add(maOf);
        panelProfil.add(angOf);
        panelProfil.add(lagDes);
        panelProfil.add(waDes);
        panelProfil.add(nemDes);
        panelProfil.add(czDes);
        panelProfil.add(maDes);
        panelProfil.add(angDes);

        mainframe.setVisible(true);

    }

    private void getOldUserInformation() {
        nameField3.setText(oldUser.getName());
        surenameField3.setText(oldUser.getSurname());
        emailField3.setText(oldUser.getMail());
        passwordField3.setText(oldUser.getPassword());
        cityComboBox3.setSelectedItem(oldUser.getCity());
        czu3.setSelected(oldUser.getCzu());
        cvut3.setSelected(oldUser.getCvut());
        ujop3.setSelected(oldUser.getUjop());
        vse3.setSelected(oldUser.getVse());
        uk3.setSelected(oldUser.getUk());
        jipka3.setSelected(oldUser.getJipka());
        angDes.setSelected(oldUser.getAngDes());
        nemDes.setSelected(oldUser.getNemDes());
        czDes.setSelected(oldUser.getCzDes());
        lagDes.setSelected(oldUser.getLagDes());
        maDes.setSelected(oldUser.getMaDes());
        waDes.setSelected(oldUser.getWaDes());

        angOf.setSelected(oldUser.getAngOf());
        nemOf.setSelected(oldUser.getNemOf());
        czOf.setSelected(oldUser.getCzOf());
        lagOf.setSelected(oldUser.getLagOf());
        maOf.setSelected(oldUser.getMaOf());
        waOf.setSelected(oldUser.getWaOf());
        oldUser.setOldMail(oldUser.getMail());
    }

    private void initEvent() {
        nameField3.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                String sag = nameField3.getText();
                if (!(((c >= 'a') || (c >= 'A')) && ((c <= 'z') || (c <= 'Z')) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
                    e.consume();
                }
            }
        });
        surenameField3.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                String sag = nameField3.getText();
                if (!(((c >= 'a') || (c >= 'A')) && ((c <= 'z')
                        || (c <= 'Z')) || (c == KeyEvent.VK_BACK_SPACE)
                        || (c == KeyEvent.VK_DELETE))) {
                    e.consume();
                }
            }
        });
        emailField3.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (!((c >= '0') && (c <= '9')
                        || ((c >= 'a') || (c >= 'A')) && ((c <= 'z') || (c <= 'Z'))
                        || (c == '@') || (c == '.')
                        || (c == KeyEvent.VK_BACK_SPACE)
                        || (c == KeyEvent.VK_DELETE))) {
                    // getToolkit().beep();
                    e.consume();
                }
            }
        });

        updateBottom.addActionListener((ActionEvent event) -> {
            klient.close();
            klient.run();
            getUserInformationForUpdate();
            validation3();
            User user = makeUserForUpdate();
            klient.sendDate(user);
            try {
                User us = (User) input.readObject();
                log.log(Level.INFO, "I have some data!!!!");
                nextStepUpdate(us);
            } catch (IOException ex) {
                Logger.getLogger(CompliteForm.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(CompliteForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        deletBottom.addActionListener((ActionEvent event) -> {
            klient.close();
            klient.run();
            User user = makeUserForDelete();
            klient.sendDate(user);
            try {
                User us = (User) input.readObject();
                log.log(Level.INFO, "I have some data!!!!");
                nextStepDelete(us);
            } catch (IOException ex) {
                Logger.getLogger(CompliteForm.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(CompliteForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        refreshBottomNM.addActionListener((ActionEvent event) -> {
            klient.close();
            klient.run();
            User user = new User();
            user.setOperationNumber(4);
            klient.sendDate(user);
            nextStepRefreshNM();
        });

        delUsBottom.addActionListener((ActionEvent event) -> {
            User user = new User();
            user.setId(idUsField.getText());
            user.setOperationNumber(3);
            user.setOldMail(oldUser.getOldMail());
            klient.close();
            klient.run();
            klient.sendDate(user);
            nextStepRefreshNM();
        });
        delUnBottom.addActionListener((ActionEvent event) -> {
            User user = new User();
            user.setId(idUnField.getText());
            user.setOperationNumber(5);
            klient.close();
            klient.run();
            klient.sendDate(user);
            nextStepRefreshNM();
        });

        relationBottom1.addActionListener((ActionEvent event) -> {
            mainframe.setSize(700, 461);
            mainframe.remove(panelProfil);
            mainframe.add(panelNM);
        });

        listBottom1.addActionListener((ActionEvent event) -> {
            mainframe.setSize(700, 462);
            mainframe.remove(panelProfil);
            mainframe.add(panelKE);
        });
        relationBottom3.addActionListener((ActionEvent event) -> {
            mainframe.setSize(700, 461);
            mainframe.remove(panelKE);
            mainframe.add(panelNM);
        });

        profilBottom3.addActionListener((ActionEvent event) -> {
            mainframe.setSize(700, 460);
            mainframe.remove(panelKE);
            mainframe.add(panelProfil);
        });
        listBottom2.addActionListener((ActionEvent event) -> {
            mainframe.setSize(700, 462);
            mainframe.remove(panelNM);
            mainframe.add(panelKE);
        });

        profilBottom2.addActionListener((ActionEvent event) -> {
            mainframe.setSize(700, 460);
            mainframe.remove(panelNM);
            mainframe.add(panelProfil);
        });

        refreshBottomKE.addActionListener((ActionEvent event) -> {
            klient.close();
            klient.run();
            User user = new User();
            user.setOperationNumber(6);
            user.setMail(oldUser.getOldMail());
            klient.sendDate(user);
            nextStepRefreshKE();
        });
    }

    private void nextStepRefreshNM() {
        try {
            User us = (User) input.readObject();
            log.log(Level.INFO, "I have some data!!!!");
            dataNM = us.getObj();
            idUnField.setText("");
            idUsField.setText("");
            if (us.getRefreshNMComplite() == true) {
                panelNM.remove(scrollPanelNM);
                tabelNM = new JTable(dataNM, headersNM);
                tabelNM = new JTable(dataNM, headersNM);
                scrollPanelNM = new JScrollPane(tabelNM);
                scrollPanelNM.setBounds(100, 70, 500, 300);
                panelNM.add(scrollPanelNM);
                if (us.getDelComplite() == true) {
                    nextStepDelete(us);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Refresh is not possible!");
            }

        } catch (IOException ex) {
            Logger.getLogger(CompliteForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CompliteForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void nextStepRefreshKE() {
        try {
            User us = (User) input.readObject();

            log.log(Level.INFO, "I have some data!!!!");
            dataKE = us.getObj();
            panelKE.remove(scrollPanelKE);
            tabelKE = new JTable(dataKE, headersKE);
            tabelKE = new JTable(dataKE, headersKE);
            scrollPanelKE = new JScrollPane(tabelKE);
            scrollPanelKE.setBounds(50, 70, 600, 300);
            panelKE.add(scrollPanelKE);

        } catch (IOException ex) {
            Logger.getLogger(CompliteForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CompliteForm.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private Boolean validation3() {
        if (nameValue.equals("")) {
            JOptionPane.showMessageDialog(null, "Enter Name");
        } else if (surenameValue.equals("")) {
            JOptionPane.showMessageDialog(null, "Enter Surname");
        } else if (emailValue.equals("")) {
            JOptionPane.showMessageDialog(null, "Enter Email");
        } else if (!(emailValue.contains("@") & emailValue.contains(".")) || emailValue.length() < 5) {
            JOptionPane.showMessageDialog(null, "Enter valid mail");
        } else if (passwordValue.equals("")) {
            JOptionPane.showMessageDialog(null, "Enter Password");
        }

        return true;
    }

    private void getUserInformationForUpdate() {
        nameValue = nameField3.getText();
        surenameValue = surenameField3.getText();
        emailValue = emailField3.getText();
        passwordValue = passwordField3.getText();
        cityValue = cityComboBox3.getSelectedItem().toString();
        cvutValue = cvut3.isSelected();
        czuValue = czu3.isSelected();
        vseValue = vse3.isSelected();
        ukValue = uk3.isSelected();
        ujopValue = ujop3.isSelected();
        jipkaValue = jipka3.isSelected();
        lagOfValue = lagOf.isSelected();
        angOfValue = angOf.isSelected();
        maOfValue = maOf.isSelected();
        czOfValue = czOf.isSelected();
        nemOfValue = nemOf.isSelected();
        waOfValue = waOf.isSelected();
        lagDesValue = lagDes.isSelected();
        angDesValue = angDes.isSelected();
        maDesValue = maDes.isSelected();
        czDesValue = czDes.isSelected();
        nemDesValue = nemDes.isSelected();
        waDesValue = waDes.isSelected();

    }

    private User makeUserForUpdate() {

        int numberOperation = 2;
        User user = new User(numberOperation, nameValue, surenameValue,
                emailValue, passwordValue, cityValue);

        user.setAngDes(angDesValue);
        user.setAngOf(angOfValue);

        user.setLagDes(lagDesValue);
        user.setLagOf(lagOfValue);

        user.setCzDes(czDesValue);
        user.setCzOf(czDesValue);

        user.setNemDes(nemDesValue);
        user.setNemOf(nemOfValue);

        user.setMaDes(maDesValue);
        user.setMaOf(maOfValue);

        user.setWaDes(waDesValue);
        user.setWaOf(waOfValue);

        user.setCzu(czuValue);
        user.setCvut(cvutValue);
        user.setUjop(ujopValue);
        user.setVse(vseValue);
        user.setUk(ukValue);
        user.setJipka(jipkaValue);

        user.setOldMail(oldUser.getOldMail());

        log.log(Level.INFO, user.toString());

        return user;

    }

    private User makeUserForDelete() {

        int numberOperation = 3;
        User user = new User();
        user.setOperationNumber(numberOperation);
        user.setOldMail(oldUser.getOldMail());
        log.log(Level.INFO, user.toString());

        return user;

    }

    private void nextStepUpdate(User user) {
        Boolean up = user.getUpdateComplite();
        if (up) {
            JOptionPane.showMessageDialog(null, "Successfully update!");
            oldUser = user;
            oldUser.setOldMail(user.getMail());

        } else {
            JOptionPane.showMessageDialog(null, "This email alredy exist!");
            getOldUserInformation();

        }
    }

    private void nextStepDelete(User user) {
        Boolean del = user.getDelComplite();
        if (del) {
            JOptionPane.showMessageDialog(null, "Your profile is successfully delete!");
            CompliteForm form = new CompliteForm();
            mainframe.dispose();

        } else {
            JOptionPane.showMessageDialog(null, "Something wrong!");
            getOldUserInformation();

        }
    }
}
