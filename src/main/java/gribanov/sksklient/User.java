/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksklient;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Represents a student information profile.
 * @author artem
 */
public class User implements Serializable{
    private static final long serialVersionUID = 8309080721495266420L;
    
    
    
    public String name, surname,mail, oldMail,password, city,id;

    public Boolean cvut, czu, vse, uk, ujop, jipka;
    public Boolean lagOf, angOf, maOf, czOf, nemOf, waOf;
    public Boolean lagDes, angDes, maDes, czDes, nemDes, waDes;
    
    public int operationNumber;
    
    public Boolean registrationCompilte;
    public Boolean loginComplite;
    public Boolean updateComplite;
    public Boolean delComplite;
    public Boolean refreshKEComplite;
    public Boolean refreshNMComplite;
    
    public Object[][] obj;
    
    /**
     *Default Constructor.
     * Creates a new user with out the given parameters
     */
    public User() {
    }
    
    /**
    * Constructor.
    * Creates a new user with the given number of operation, name, surname,
    * mail,password and city.
    * 
    * @param operationNumber the number of operation to set
    * @param mail the mail to set
    * @param password the password to set
    * @param name the name to set
    * @param surname the surname to set
    * @param city the city to set
    */ 
    public User(int operationNumber,String name, String surname, String mail, 
            String password, String city) {
        this.operationNumber = operationNumber;
        this.name = name;
        this.surname = surname;
        this.mail = mail;
        this.password = password;
        this.city = city;
                
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param surname the surname to set
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * @param mail the mail to set
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * @param oldMail the oldMail to set
     */
    public void setOldMail(String oldMail) {
        this.oldMail = oldMail;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @param cvut the cvut to set
     */
    public void setCvut(Boolean cvut) {
        this.cvut = cvut;
    }

    /**
     * @param czu the czu to set
     */
    public void setCzu(Boolean czu) {
        this.czu = czu;
    }

    /**
     * @param vse the vse to set
     */
    public void setVse(Boolean vse) {
        this.vse = vse;
    }

    /**
     * @param uk the uk to set
     */
    public void setUk(Boolean uk) {
        this.uk = uk;
    }

    /**
     * @param ujop the ujop to set
     */
    public void setUjop(Boolean ujop) {
        this.ujop = ujop;
    }

    /**
     * @param jipka the jipka to set
     */
    public void setJipka(Boolean jipka) {
        this.jipka = jipka;
    }

    /**
     * @param lagOf the lagOf to set
     */
    public void setLagOf(Boolean lagOf) {
        this.lagOf = lagOf;
    }

    /**
     * @param angOf the angOf to set
     */
    public void setAngOf(Boolean angOf) {
        this.angOf = angOf;
    }

    /**
     * @param maOf the maOf to set
     */
    public void setMaOf(Boolean maOf) {
        this.maOf = maOf;
    }

    /**
     * @param czOf the czOf to set
     */
    public void setCzOf(Boolean czOf) {
        this.czOf = czOf;
    }

    /**
     * @param nemOf the nemOf to set
     */
    public void setNemOf(Boolean nemOf) {
        this.nemOf = nemOf;
    }

    /**
     * @param waOf the waOf to set
     */
    public void setWaOf(Boolean waOf) {
        this.waOf = waOf;
    }

    /**
     * @param lagDes the lagDes to set
     */
    public void setLagDes(Boolean lagDes) {
        this.lagDes = lagDes;
    }

    /**
     * @param angDes the angDes to set
     */
    public void setAngDes(Boolean angDes) {
        this.angDes = angDes;
    }

    /**
     * @param maDes the maDes to set
     */
    public void setMaDes(Boolean maDes) {
        this.maDes = maDes;
    }

    /**
     * @param czDes the czDes to set
     */
    public void setCzDes(Boolean czDes) {
        this.czDes = czDes;
    }

    /**
     * @param nemDes the nemDes to set
     */
    public void setNemDes(Boolean nemDes) {
        this.nemDes = nemDes;
    }

    /**
     * @param waDes the waDes to set
     */
    public void setWaDes(Boolean waDes) {
        this.waDes = waDes;
    }

     /**
     * @param  operationNumber the operationNumber  to set
     */
    public void setOperationNumber(int operationNumber) {
        this.operationNumber = operationNumber;
    }

    /**
     * @param registrationCompilte the registrationComplite to set
     */
    public void setRegistrationCompilte(Boolean registrationCompilte) {
        this.registrationCompilte = registrationCompilte;
    }

    /**
     * @param loginComplite the loginComplite to set
     */
    public void setLoginComplite(Boolean loginComplite) {
        this.loginComplite = loginComplite;
    }

    /**
     * @param updateComplite the updateComplite to set
     */
    public void setUpdateComplite(Boolean updateComplite) {
        this.updateComplite = updateComplite;
    }

    /**
     * @param delComplite the to delComplite set
     */
    public void setDelComplite(Boolean delComplite) {
        this.delComplite = delComplite;
    }

    /**
     * @param refreshKEComplite the refreshKEComplite to set
     */
    public void setRefreshKEComplite(Boolean refreshKEComplite) {
        this.refreshKEComplite = refreshKEComplite;
    }

    /**
     * @param refreshNMComplite the refreshNMComplite to set
     */
    public void setRefreshNMComplite(Boolean refreshNMComplite) {
        this.refreshNMComplite = refreshNMComplite;
    }

    /**
     * @param obj the obj to set
     */
    public void setObj(Object[][] obj) {
        this.obj = obj;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @return the oldMail
     */
    public String getOldMail() {
        return oldMail;
    }
    
    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the cvut
     */
    public Boolean getCvut() {
        return cvut;
    }

    /**
     * @return the czu
     */
    public Boolean getCzu() {
        return czu;
    }

    /**
     * @return the vse
     */
    public Boolean getVse() {
        return vse;
    }

    /**
     * @return the uk
     */
    public Boolean getUk() {
        return uk;
    }

    /**
     * @return the ujop
     */
    public Boolean getUjop() {
        return ujop;
    }

    /**
     * @return the jipka
     */
    public Boolean getJipka() {
        return jipka;
    }

    /**
     * @return the lagOf
     */
    public Boolean getLagOf() {
        return lagOf;
    }

    /**
     * @return the angOf
     */
    public Boolean getAngOf() {
        return angOf;
    }

    /**
     * @return the maOf
     */
    public Boolean getMaOf() {
        return maOf;
    }

    /**
     * @return the czOf
     */
    public Boolean getCzOf() {
        return czOf;
    }

    /**
     * @return the nemOf
     */
    public Boolean getNemOf() {
        return nemOf;
    }

    /**
     * @return the waOf
     */
    public Boolean getWaOf() {
        return waOf;
    }

    /**
     * @return the lagDes
     */
    public Boolean getLagDes() {
        return lagDes;
    }

    /**
     * @return the angDes
     */
    public Boolean getAngDes() {
        return angDes;
    }

    /**
     * @return the maDes
     */
    public Boolean getMaDes() {
        return maDes;
    }

    /**
     * @return the czDes
     */
    public Boolean getCzDes() {
        return czDes;
    }

    /**
     * @return the waDes
     */
    public Boolean getNemDes() {
        return nemDes;
    }

    /**
     * @return the waDes
     */
    public Boolean getWaDes() {
        return waDes;
    }

    /**
     * @return the operationNumber
     */
    public int getOperationNumber() {
        return operationNumber;
    }

    /**
     * @return the registrationCompilte
     */
    public Boolean getRegistrationCompilte() {
        return registrationCompilte;
    }

    /**
     * @return the loginComplite
     */
    public Boolean getLoginComplite() {
        return loginComplite;
    }

    /**
     * @return the updateComplite
     */
    public Boolean getUpdateComplite() {
        return updateComplite;
    }

    /**
     * @return the delComplite
     */
    public Boolean getDelComplite() {
        return delComplite;
    }

    /**
     * @return the refreshKEComplite
     */
    public Boolean getRefreshKEComplite() {
        return refreshKEComplite;
    }
    /**
     * @return the refreshNMComplite
     */
    public Boolean getRefreshNMComplite() {
        return refreshNMComplite;
    }
    
    /**
     * @return the obj
     */
    public Object[][] getObj() {
        return obj;
    }
}