/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksklient;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Client socket.
 * @author artem
 */
public class Klient implements Runnable{
    private static Logger log = Logger.getLogger(Klient.class.getName());
    static private Socket connetcion;
    static private ObjectOutputStream output;
    static public ObjectInputStream input;

    
    public static void main(String[] args) {
        
    }

    /**
     * The method run a socket client.
     */
    @Override
    public void run() {
        try { 
            connetcion = new Socket(InetAddress.getByName("127.0.0.1"), 4567);
                log.log(Level.INFO, "Start klient....................");
                log.log(Level.INFO, "Find connection with Server....................");
                output = new ObjectOutputStream(connetcion.getOutputStream());
                input = new ObjectInputStream(connetcion.getInputStream());
                log.log(Level.INFO, "Waiting server data....................");

        } catch (UnknownHostException e) {
            log.log(Level.INFO,"Don't know about host: localhost.");
            System.exit(1);
        } catch (IOException e) {
            log.log(Level.INFO,"Couldn't get I/O for the connection to: localhost.");
            System.exit(1);
        } 
    }
    
    /**
     * The method send data on server.
     * @param obj the object to send
     */
    public void  sendDate(Object obj){
        try {
            output.flush();
            output.writeObject(obj);
            log.log(Level.INFO, "Object is sended on server....................");
        } catch (Exception e) {
        }
    }
   
    /**
     * The method close connection with server.
     */
    public void close(){
        try {
            connetcion.close();
            log.log(Level.INFO, "connetcion close....................");
        } catch (IOException ex) {
            Logger.getLogger(Klient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
