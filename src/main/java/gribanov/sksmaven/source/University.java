/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.source;

import gribanov.sksmaven.enumClass.EnumUniversity;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


/**
 * Represents a university.
 * A university can have a lot of students.
 * @author artem
 */
@Entity
@Table
public class University implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idUn;
    
    @Column(unique = true) 
    private  EnumUniversity university;
    
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable
    private Set<SKSuser> users = new HashSet<SKSuser>();


    /**
     *Default Constructor.
     * Creates a new university with out the given parameters
     */
    public University() {
    }

    /**
    * Creates a new university with the given id,subject and student.
    * 
    * @param idUniversity the university id to set
    * @param name the university name to set
    * @return the university
    */
    public static University createUniversity(int idUniversity, EnumUniversity name){
        University university = new University();
        university.setId(idUniversity);
        university.setUniversity(name);
        
        return university;
        
    
    }
    

    /**
     * @param user the user to add
     */
    public void addStudent(SKSuser user){
        users.add(user);
    }
    
    /**
     * @param user the user to remove
     */
    public void deleteStudent(SKSuser user){
        users.remove(user);
    }
    
    /**
     * @return the users
     */
    public Set<SKSuser> getStudents() {
        return users;
    }

    /**
     * @return the id
     */
    public int getId() {
        return idUn;
    }

    
    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.idUn = id;
    }
    
    /**
     * @param university the university to set
     */
    public void setUniversity(EnumUniversity university) {
        this.university = university;
    }
    
    /**
     * @return the university
     */
    public EnumUniversity getUniversity() {
        return university;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + this.idUn;
        hash = 83 * hash + Objects.hashCode(this.university);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final University other = (University) obj;
        if (this.idUn != other.idUn) {
            return false;
        }
        return true;
    }



    @Override
    public String toString() {
        return "source.University[ id=" + idUn + " ]";
    }
    
}
