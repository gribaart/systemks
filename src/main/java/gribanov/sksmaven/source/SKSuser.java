/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.source;

import gribanov.sksmaven.enumClass.City;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column; 
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table; 



 


/**
 * Represents a student enrolled in the university.
 * A student can be enrolled in many university.
 * A student has to offer and require a lot of subject.
 * @author artem
 */
@Entity
@Table
public class SKSuser implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDUSER")
    private int idUser;
    
    @Column(name = "EMAIL",unique = true) 
    private  String mail;
    
    @Column(name = "PASSWORD") 
    private  String password;
    
    @Column(name = "NAME") 
    private  String name;
    
    @Column(name = "SURENAME") 
    private  String surename;
    
    @Column(name = "CITY") 
    private  City city;       
    
    @OneToMany(mappedBy = "userRelation", cascade = javax.persistence.CascadeType.ALL,fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<OfferKnowledge> offers;
    
    @OneToMany(mappedBy = "userRelation", cascade = javax.persistence.CascadeType.ALL,fetch = FetchType.EAGER, orphanRemoval = true)
    private Set<DesireKnowledge> desires;
    
    @ManyToMany(mappedBy = "users", fetch = FetchType.EAGER)
    private Set<University> universitys;
    
    /**
    * Creates a new SKSuser with the given id,name,surname,
    * mail,password and city.
    * 
    * @param idUser the idUser to set
    * @param mail the mail to set
    * @param password the password to set
    * @param name the name to set
    * @param surename the surname to set
    * @param city the city to set
    * @return the user
    */
    public static SKSuser crateNewUser(int idUser, String mail, String password, String name, String surename, City city) {
        SKSuser user = new SKSuser();
        user.setId(idUser);
        user.setMail(mail);
        user.setPassword(password);
        user.setName(name);
        user.setSurename(surename);
        user.setCity(city);

        return user;
    }
   

    /**
     * @param idUser the idUser to set
     */
    public void setId(int idUser) {
        this.idUser = idUser;
    }

    /**
     * @param mail the mail to set
     */
    public void setMail(String mail) {
        this.mail = mail;
    }
    
    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param surename the surname to set
     */
    public void setSurename(String surename) {
        this.surename = surename;
    }

    /**
     * @param city the city to set
     */
    public void setCity(City city) {
        this.city = city; 
     }


    /**
     * @param offers the offers to set
     */
    public void setOffers(OfferKnowledge offers) {
        this.offers.add(offers);
    }
    
    /**
     * @param desire the desire to set
     */
    public void setDesires(DesireKnowledge desire) {
        this.desires.add(desire);
    }
    
    /**
     * @return the offers
     */
    public Set<OfferKnowledge> getOffers() {
        return offers;
    }

    /**
     * @return the desires
     */
    public Set<DesireKnowledge> getDesires() {
        return desires;
    }
    
    /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @return the idUser
     */
    public int getId() {
        return idUser;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }
    
    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the surname
     */
    public String getSurename() {
        return surename;
    }

    /**
     * @return the city
     */
    public City getCity() {
        return city;
    }

    /**
     * @return the university
     */
    public Set<University> getUniversitys() {
        return universitys;
    }
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.idUser;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SKSuser other = (SKSuser) obj;
        if (!Objects.equals(this.mail, other.mail)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "source.User[ id=" + idUser + " ]";
    }
    
}