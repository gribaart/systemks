/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.source;

import gribanov.sksmaven.enumClass.Mark;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Represents a comment that can send each other users.
 * @author artem
 */
@Entity
@Table
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idComment", unique = true, nullable = false)
    private int idComment;

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date created;
    
    @Column 
    private  String text;
    
    @Column
    private Mark mark;
    
    @JoinColumn
    @OneToOne(cascade = CascadeType.REMOVE)
    private SKSuser senderRelation;
    
    @JoinColumn
    @OneToOne(cascade = CascadeType.REMOVE)
    private SKSuser adresseeRelation;
    
    /**
     *Default Constructor.
     * Creates a new comment with out the given parameters
     */
    public Comment() {
        onCreate();
    }

    /**
    * Creates a new massage with the given id,text, sender and addressee users.
    * @param idComment the comment id to set
    * @param text the text to set
    * @param sender the sender to set
    * @param adressee the addressee to set
    * @param mark the mark to set
    * @return the comment
    */
    public static Comment crateComment(Integer idComment, String text, SKSuser sender, SKSuser adressee, Mark mark) {
        Comment comment = new Comment();
        comment.setId(idComment);
        comment.setText(text);
        comment.setSenderRelation(sender);
        comment.setAdresseeRelation(adressee);
        comment.setMak(mark);
        
        return comment;
    }
    

    @PrePersist
    protected void onCreate() {
    created = new Date();
    }
    
    /**
     * @param idComment the idComment to set
     */
    public void setId(int idComment) {
        this.idComment = idComment;
    }


    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    
    /**
     * @param mark the mark to set
     */
    public void setMak(Mark mark) {
        this.mark = mark;
    }
    
    
     /**
     * @return the adresseeRelation
     */
    public SKSuser getAdresseeRelation() {
        return adresseeRelation;
    }

    /**
     * @param adresseeRelation the adresseeRelation to set
     */
    public void setAdresseeRelation(SKSuser adresseeRelation) {
        this.adresseeRelation = adresseeRelation;
    }
    
    /**
     * @return the senderRelation
     */
    public SKSuser getSenderRelation() {
        return senderRelation;
    }

    /**
     * @param senderRelation the senderRelation to set
     */
    public void setSenderRelation(SKSuser senderRelation) {
        this.senderRelation = senderRelation;
    }
    
    
    
    /**
     * @return the mark
     */
    public Mark getMark() {
        return mark;
    }
    
    /**
     * @return the idComment
     */
    public int getId() {
        return idComment;
    }


    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }




    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.created);
        hash = 89 * hash + Objects.hashCode(this.senderRelation);
        hash = 89 * hash + Objects.hashCode(this.adresseeRelation);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Comment other = (Comment) obj;
        if (!Objects.equals(this.created, other.created)) {
            return false;
        }
        if (!Objects.equals(this.senderRelation, other.senderRelation)) {
            return false;
        }
        if (!Objects.equals(this.adresseeRelation, other.adresseeRelation)) {
            return false;
        }
        if (this.mark != other.mark) {
            return false;
        }
        return true;
    }
    

    

    @Override
    public String toString() {
        return "source.Comment[ id=" + idComment + " ]";
    }
    
}