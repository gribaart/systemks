/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.source;

import gribanov.sksmaven.enumClass.Subject;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Represents a subject proposed by students.
 * @author artem
 */
@Entity
@Table
public class OfferKnowledge implements Serializable {

    private static final long serialVersionUID = 2L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private int idOffer;

    @Column 
    private Subject subject;
    
    @ManyToOne(fetch = FetchType.EAGER)
    private SKSuser userRelation; 

    /**
     *Default Constructor.
     * Creates a new offer subject with out the given parameters
     */
    public OfferKnowledge() {
    }
    
    /**
    * Creates a new offer knowledge with the given id,subject and student.
    * 
    * @param idOffer the offer id to set
    * @param subject the subject to set
    * @param user the user to set
    * @return the offer
    */
    public static  OfferKnowledge createNewOffer(int idOffer, Subject subject, SKSuser user) {
        OfferKnowledge offer = new OfferKnowledge();
        offer.idOffer = idOffer;
        offer.subject = subject;
        offer.userRelation = user;
        return offer;
    }       
    
    /**
     * @return the idOffer
     */
    public int getId() {
        return idOffer;
    }
    
    
    /**
     * @param idOffer the idOffer to set
     */
    public void setId(int idOffer) {
        this.idOffer = idOffer;
    }

    /**
     * @return the subject
     */
    public Subject getSubject() {
        return subject;
    }
    
    /**
     * @param subject the subject to set
     */
    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    /**
     * @return the userRelation
     */
    public SKSuser getUserRelation() {
        return userRelation;
    }

    /**
     * @param userRelation the userRelation to set
     */
    public void setUserRelation(SKSuser userRelation) {
        this.userRelation = userRelation;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.subject);
        hash = 59 * hash + Objects.hashCode(this.userRelation);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OfferKnowledge other = (OfferKnowledge) obj;
        if (this.subject != other.subject) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "source.OfferKnowledge[ id=" + idOffer + " ] " + subject.toString();
    }
    
}
