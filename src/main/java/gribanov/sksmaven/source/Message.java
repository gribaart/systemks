/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.source;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

/**
 * Represents a message that can send each other users.
 * @author artem
 */
@Entity
@Table
public class Message implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private int idMessage;

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    private Date created;
    
    @Column
    private  String text;
    
    @JoinColumn
    @OneToOne
    private SKSuser senderRelation;
    
    @JoinColumn
    @OneToOne
    private SKSuser adresseeRelation;

    /**
     *Default Constructor.
     * Creates a new message with out the given parameters
     */
    public Message() {
        onCreate();
    }

    /**
    * Creates a new massage with the given id,text, sender and addressee users.
    * @param idMessage the message id to set
    * @param text the text to set
    * @param sender the sender to set
    * @param adressee the addressee to set
    * @return the message
    */
    public static Message crateMessage(int idMessage, String text, SKSuser sender, SKSuser adressee) {
        Message message = new Message();
        message.setId(idMessage);
        message.setText(text);
        message.setSenderRelation(sender);
        message.setAdresseeRelation(adressee);
        
        return message;
    }
    
    /**
     * @return the adresseeRelation
     */
    public SKSuser getAdresseeRelation() {
        return adresseeRelation;
    }

    /**
     * @param adresseeRelation the adresseeRelation to set
     */
    public void setAdresseeRelation(SKSuser adresseeRelation) {
        this.adresseeRelation = adresseeRelation;
    }
    
    /**
     * @return the senderRelation
     */
    public SKSuser getSenderRelation() {
        return senderRelation;
    }

    /**
     * @param senderRelation the senderRelation to set
     */
    public void setSenderRelation(SKSuser senderRelation) {
        this.senderRelation = senderRelation;
    }
    
    
    @PrePersist
    protected Date onCreate() {
    created = new Date();
    return created;
    }
    
    /**
     * @param idMessage the idMessage to set
     */
    public void setId(int idMessage) {
        this.idMessage = idMessage;
    }


    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }
    
    
    /**
     * @return the idMessage
     */
    public int getId() {
        return idMessage;
    }


    /**
     * @return the created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }


    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + this.idMessage;
        hash = 13 * hash + Objects.hashCode(this.created);
        hash = 13 * hash + Objects.hashCode(this.text);
        hash = 13 * hash + Objects.hashCode(this.senderRelation);
        hash = 13 * hash + Objects.hashCode(this.adresseeRelation);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Message other = (Message) obj;
        if (!Objects.equals(this.senderRelation, other.senderRelation)) {
            return false;
        }
        if (!Objects.equals(this.adresseeRelation, other.adresseeRelation)) {
            return false;
        }
        return true;
    }
    



    @Override
    public String toString() {
        return "source.Message[ id=" + idMessage + text + " ]";
    }
    
}