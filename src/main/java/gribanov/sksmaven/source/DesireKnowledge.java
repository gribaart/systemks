/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.source;

import gribanov.sksmaven.enumClass.Subject;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Represents a subject that is sought by students.
 * @author artem
 */
@Entity
@Table
public class DesireKnowledge implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private int idDesire;

    @Column
    private Subject subject;

    @ManyToOne(fetch = FetchType.EAGER)
    private SKSuser userRelation; 
    
    /**
     *Default Constructor.
     * Creates a new desire subject with out the given parameters
     */
    public DesireKnowledge() {
    }

    /**
    * Creates a new desire knowledge with the given id,subject and student.
    * 
    * @param idDesire the desire id to set
    * @param subject the subject to set
    * @param user the user to set
    * @return the offer
    */
    public static  DesireKnowledge createNewDesire(int idDesire, Subject subject, SKSuser user) {
        DesireKnowledge desire = new DesireKnowledge();
        desire.idDesire = idDesire;
        desire.subject = subject;
        desire.userRelation = user;
        return desire;
    }
    
    

    /**
     * @return the idDesire
     */
    public int getId() {
        return idDesire;
    }

    /**
     * @param idDesire the idDesire to set
     */
    public void setId(int idDesire) {
        this.idDesire = idDesire;
    }

    /**
     * @return the subject
     */
    public Subject getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    /**
     * @return the userRelation
     */
    public SKSuser getUserRelation() {
        return userRelation;
    }

    /**
     * @param userRelation the userRelation to set
     */
    public void setUserRelation(SKSuser userRelation) {
        this.userRelation = userRelation;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.subject);
        hash = 23 * hash + Objects.hashCode(this.userRelation);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DesireKnowledge other = (DesireKnowledge) obj;
        if (this.subject != other.subject) {
            return false;
        }
        return true;
    }


    
    

    @Override
    public String toString() {
        return "source.DesireKnowledge[ id=" + idDesire + " ] " + subject.name() ;
    }
    
}
