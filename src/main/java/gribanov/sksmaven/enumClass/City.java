/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.enumClass;

/**
 * Represents a city.
 * @author artem
 */
public enum City {
    PRAHA("Praha",1),
    BRNO("Brno",2),
    OSTRAVA("Ostrava",3),
    PLZEN("Plzen",4),
    LIBIREC("Libirec",5),
    OLOMOUC("Olomouc",6);
    
    private String name;
    private int cityId;

    private City(String name, int cityId) {
        this.name = name;
        this.cityId = cityId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    

    @Override
    public String toString() {
        return "City{" + "name=" + name + '}';
    }
    
    
}
