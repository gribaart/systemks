/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.enumClass;

/**
 * Represents a university name.
 * @author artem
 */
public enum EnumUniversity {
    CZU("CZU",0),
    CVUT("CVUT",1),
    UJOP("UJOP",2),
    VSE("VSE",3),
    UK("UK",4),
    JIPKA("JIPKA",5);
    
    private String name;
    private int unId;

    private EnumUniversity(String name, int unId) {
        this.name = name;
        this.unId = unId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the id
     */
    public int getUnId() {
        return unId;
    }
    

    @Override
    public String toString() {
        return "University{" + "name=" + name + '}';
    }
    
    
}
