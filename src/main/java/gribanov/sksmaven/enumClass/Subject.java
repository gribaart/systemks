/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.enumClass;

/**
 * Represents a subject.
 * @author artem
 */
public enum Subject {
    
    ANG("Anglický jazyk",0),
    NEM("Německý jazyk",1),
    CZ("Český jazyk",2),
    LAG("Lineární algebra",3),
    MA("Matematická analiza",4),
    WA("Webové aplikace",5);

    private String name;
    private int subjectId;

    private Subject(String name, int subjectId) {
        this.name = name;
        this.subjectId = subjectId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the name
     */
    public int getSubjectId() {
        return subjectId;
    }
    
    
    

    @Override
    public String toString() {
        return "Subject{" + "name=" + name + ", subjectId=" + subjectId + '}';
    }
    
    
    
}
