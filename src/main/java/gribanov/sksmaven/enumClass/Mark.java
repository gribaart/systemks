/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.enumClass;

/**
 * Represents a mark.
 * @author artem
 */
public enum Mark {
    POSITIVE("POSITIVE",0),
    NEGATIVE("NEGATIVE",1);

    
    private String name;
    private int markId;

    private Mark(String name, int markId) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Mark{" + "name=" + name + '}';
    }
}
