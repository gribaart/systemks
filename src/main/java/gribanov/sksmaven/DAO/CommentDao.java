/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.DAO;

import gribanov.sksmaven.derby.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import gribanov.sksmaven.source.Comment;

/**
 * This class is responsible to interact with comment data from a database.
 *
 * @author artem
 */
public class CommentDao {

    private static Logger log = Logger.getLogger(CommentDao.class.getName());

    /**
     * The method will bring a new comment to the database
     */
    public void addComment(Comment comment) {
        log.info("----->Hibernate + PostgreSQL + addComment<-----");

        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            session.save(comment);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------comment ADD complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
    }

    /**
     * The method removes the comment from the database
     */
    public void deleteComment(int commentId) {
        log.info("----->Hibernate + PostgreSQL + deleteComment<-----");
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            Comment comment = (Comment) session.load(Comment.class, new Integer(commentId));
            session.delete(comment);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------comment DELETED complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
    }
    
    /**
     * The method returns a list of all comments.
     * @return comments
     */
    public List<Comment> getAllComments() {
        log.info("----->Hibernate + PostgreSQL + getAllComment<-----");
        List<Comment> comments = new ArrayList<Comment>();
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            comments = session.createQuery("from Comment").list();

        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
        return comments;
    }

    /**
     * The method looks up the comment on ID number.
     * @param commentId
     * @return comment
     */
    public Comment getCommentById(int commentId) {
        log.info("----->Hibernate + PostgreSQL + getCommentById<-----");
        Comment comment = null;
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Comment where idComment = :idComment";
            Query query = session.createQuery(queryString);
            query.setInteger("idComment", commentId);
            comment = (Comment) query.uniqueResult();
            log.log(Level.INFO, "--------------Get comment by id complite");
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
        return comment;
    }
}
