/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.DAO;

import gribanov.sksmaven.derby.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import gribanov.sksmaven.source.DesireKnowledge;
import gribanov.sksmaven.source.SKSuser;

/**
 * This class is responsible to interact with desire knowledge data from a database.
 *
 * @author artem
 */
public class DesireKnowledgeDao {

    private static Logger log = Logger.getLogger(DesireKnowledgeDao.class.getName());

    /**
     * The method will bring a new desire subject to the database
     */
    public void addDesire(DesireKnowledge desire) {
        log.info("----->Hibernate + PostgreSQL + addDesire<-----");

        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            session.save(desire);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------desire ADD complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
    }

    /**
     * The method removes the desire subject from the database
     */
    public void deleteDesireKnowledge(int desireId) {
        log.info("----->Hibernate + PostgreSQL + deleteDesire<-----");
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            DesireKnowledge desire = (DesireKnowledge) session.load(DesireKnowledge.class, new Integer(desireId));

            SKSuser user = desire.getUserRelation();
            user.getDesires().remove(desire);

            session.delete(desire);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------desire DELETED complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
    }

    /**
     * The method returns a list of all desires subjects.
     * @return desires
     */
    public List<DesireKnowledge> getAllDesireKnowledge() {
        log.info("----->Hibernate + PostgreSQL + getAllDesire<-----");
        List<DesireKnowledge> desires = new ArrayList<DesireKnowledge>();
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            desires = session.createQuery("from DesireKnowledge").list();
            log.log(Level.INFO, "---------------offer get All complite.");

        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
        return desires;
    }
}
