/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.DAO;

import gribanov.sksmaven.derby.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import gribanov.sksmaven.source.SKSuser;
import gribanov.sksmaven.source.University;

/**
 * This class is responsible to interact with user data from a database.
 *
 * @author artem
 */
public class UserDao {

    private static Logger log = Logger.getLogger(UserDao.class.getName());

    /**
     * The method will bring a new user to the database
     */
    public void addUser(SKSuser user) {
        log.info("----->Hibernate + PostgreSQL + addUser<-----");

        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------User ADD complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
    }

    /**
     * The method removes the user from the database
     */
    public void deleteUser(int userid) {
        log.info("----->Hibernate + PostgreSQL + deleteUser<-----");
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            SKSuser user = (SKSuser) session.load(SKSuser.class, new Integer(userid));

            for (University university : user.getUniversitys()) {
                university.getStudents().remove(user);
            }

            session.delete(user);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------User DELETED complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
    }

    /**
     * The method updates the user information in the database.
     * @param user 
     */
    public void updateUser(SKSuser user) {
        log.info("----->Hibernate + PostgreSQL + updateUser<-----");
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------User UPDATE complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "---------------SESSION CLOSED.");
        }
    }

    /**
     * The method returns a list of all users.
     * @return users
     */
    public List<SKSuser> getAllUsers() {
        log.info("----->Hibernate + PostgreSQL + getAllUsers<-----");
        List<SKSuser> users = new ArrayList<SKSuser>();
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            users = session.createQuery("from SKSuser").list();

        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
        return users;
    }

    /**
     * The method looks up the user on ID number.
     * @param userid
     * @return user
     */
    public SKSuser getUserById(int userid) {
        log.info("----->Hibernate + PostgreSQL + getUserById<-----");
        SKSuser user = null;
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from SKSuser where idUser = :idUser";
            Query query = session.createQuery(queryString);
            query.setInteger("idUser", userid);
            user = (SKSuser) query.uniqueResult();
            log.log(Level.INFO, "--------------Get user by id complite");
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
        return user;
    }

    /**
     * The method looks up the user on email.
     * @param email
     * @return user
     */
    public SKSuser getUserByEmail(String email) {
        log.info("----->Hibernate + PostgreSQL + getUserByEmail<-----");
        SKSuser user = null;
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from SKSuser where email = :email";
            Query query = session.createQuery(queryString);
            query.setString("email", email);
            user = (SKSuser) query.uniqueResult();
            log.log(Level.INFO, "--------------Get user by email complite");
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
        return user;
    }
}
