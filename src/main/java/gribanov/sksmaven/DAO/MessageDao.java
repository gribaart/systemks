/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.DAO;

import gribanov.sksmaven.derby.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import gribanov.sksmaven.source.Message;

/**
 * This class is responsible to interact with message data from a database.
 *
 * @author artem
 */
public class MessageDao {

    private static Logger log = Logger.getLogger(MessageDao.class.getName());

    /**
     * The method will bring a new message to the database
     */
    public void addMessage(Message message) {
        log.info("----->Hibernate + PostgreSQL + addMessage<-----");

        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            session.save(message);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------Message ADD complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
    }

    /**
     * The method removes the message from the database
     */
    public void deleteMessage(int messageId) {
        log.info("----->Hibernate + PostgreSQL + deleteMessage<-----");
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            Message message = (Message) session.load(Message.class, new Integer(messageId));
            session.delete(message);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------Message DELETED complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
    }
    
    /**
     * The method returns a list of all messages.
     * @return messages
     */
    public List<Message> getAllMessages() {
        log.info("----->Hibernate + PostgreSQL + getAllMessage<-----");
        List<Message> messages = new ArrayList<Message>();
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            messages = session.createQuery("from Message").list();

        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
        return messages;
    }

    /**
     * The method looks up the message on ID number.
     * @param messageId
     * @return message
     */
    public Message getMessageById(int messageId) {
        log.info("----->Hibernate + PostgreSQL + getMessageById<-----");
        Message message = null;
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from Message where idMessage = :idMessage";
            Query query = session.createQuery(queryString);
            query.setInteger("idMessage", messageId);
            message = (Message) query.uniqueResult();
            log.log(Level.INFO, "--------------Get message by id complite");
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
        return message;
    }
}
