/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.DAO;

import gribanov.sksmaven.derby.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import gribanov.sksmaven.source.SKSuser;
import gribanov.sksmaven.source.University;

/**
 * This class is responsible to interact with university data from a database.
 *
 * @author artem
 */
public class UniversityDao {

    private static Logger log = Logger.getLogger(UniversityDao.class.getName());

    /**
     * The method will bring a new university to the database
     */
    public void addUniversity(University university) {
        log.info("----->Hibernate + PostgreSQL + addUniversity<-----");
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            session.save(university);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------university ADD complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
    }

    /**
     * The method removes the university from the database
     */
    public void deleteUniversity(int universityId) {
        log.info("----->Hibernate + PostgreSQL + deleteUniversity<-----");
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            University university = (University) session.load(University.class, new Integer(universityId));

            for (SKSuser user : university.getStudents()) {
                user.getUniversitys().remove(university);
            }

            session.delete(university);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------university DELETED complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
    }

    /**
     * The method updates the university information in the database.
     * @param university 
     */
    public void updateUniversity(University university) {
        log.info("----->Hibernate + PostgreSQL + updateUniversity<-----");
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            session.update(university);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------University UPDATE complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "---------------SESSION CLOSED.");
        }
    }
    
    /**
     * The method returns a list of all university.
     * @return university
     */
    public List<University> getAllUniversity() {
        log.info("----->Hibernate + PostgreSQL + getAllUniversity<-----");
        List<University> universitys = new ArrayList<University>();
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            universitys = session.createQuery("from University").list();

        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
        return universitys;
    }

    /**
     * The method looks up the university on ID number.
     * @param universityId
     * @return university
     */
    public University getUniversityById(int universityId) {
        log.info("----->Hibernate + PostgreSQL + getUniversityById<-----");
        University university = null;
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            String queryString = "from University where university = :idun";
            Query query = session.createQuery(queryString);
            query.setInteger("idun", universityId);
            university = (University) query.uniqueResult();
        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
        return university;
    }
}
