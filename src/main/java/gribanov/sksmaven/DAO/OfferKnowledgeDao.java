/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.DAO;

import gribanov.sksmaven.derby.util.HibernateUtil;
import gribanov.sksmaven.source.DesireKnowledge;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import gribanov.sksmaven.source.OfferKnowledge;
import gribanov.sksmaven.source.SKSuser;
import org.hibernate.Hibernate;

/**
 * This class is responsible to interact with offer knowledge data from a database.
 *
 * @author artem
 */
public class OfferKnowledgeDao {

    private static Logger log = Logger.getLogger(OfferKnowledgeDao.class.getName());

    /**
     * The method will bring a new offer subject to the database
     */
    public void addOffer(OfferKnowledge offer) {
        log.info("----->Hibernate + PostgreSQL + addOffer<-----");

        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            session.save(offer);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------offer ADD complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
    }

    /**
     * The method removes the offer subject from the database
     */
    public void deleteOfferKnowledge(int offerId) {
        log.info("----->Hibernate + PostgreSQL + deleteOffer<-----");
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            OfferKnowledge offer = (OfferKnowledge) session.load(OfferKnowledge.class, new Integer(offerId));

            SKSuser user = offer.getUserRelation();
            user.getOffers().remove(offer);

            session.delete(offer);
            session.getTransaction().commit();
            log.log(Level.INFO, "---------------offer DELETED complite.");
        } catch (RuntimeException e) {
            if (trns != null) {
                trns.rollback();
            }
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
    }
    
    /**
     * The method returns a list of all offer subjects.
     * @return users
     */
    public List<OfferKnowledge> getAllOfferKnowledge() {
        log.info("----->Hibernate + PostgreSQL + getAllOffer<-----");
        List<OfferKnowledge> offers = new ArrayList<OfferKnowledge>();
        Transaction trns = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            trns = session.beginTransaction();
            offers = session.createQuery("from DesireKnowledge").list();
            log.log(Level.INFO, "---------------offer get All complite.");

        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            session.flush();
            session.close();
            log.log(Level.INFO, "------------Session close.");
        }
        return offers;
    }
}
