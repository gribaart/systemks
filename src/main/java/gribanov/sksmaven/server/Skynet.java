/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.server;

import gribanov.sksklient.User;
import gribanov.sksmaven.DAO.DesireKnowledgeDao;
import gribanov.sksmaven.DAO.OfferKnowledgeDao;
import gribanov.sksmaven.DAO.UniversityDao;
import gribanov.sksmaven.DAO.UserDao;
import gribanov.sksmaven.SearchKnowledgeExenge.KnowledgeExchange;
import gribanov.sksmaven.enumClass.City;
import gribanov.sksmaven.enumClass.EnumUniversity;
import gribanov.sksmaven.enumClass.Subject;
import gribanov.sksmaven.source.DesireKnowledge;
import gribanov.sksmaven.source.OfferKnowledge;
import gribanov.sksmaven.source.SKSuser;
import gribanov.sksmaven.source.University;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a socket server.
 *
 * @author artem
 */
public class Skynet implements Runnable {

    private static Logger log = Logger.getLogger(Skynet.class.getName());
    static private ServerSocket server;
    static private Socket connetcion;
    static private ObjectOutputStream output;
    static private ObjectInputStream input;

    public static void main(String[] args) {
        new Thread(new Skynet()).run();
    }

    /**
     * The method run a socket server.
     */
    @Override
    public void run() {
        try {
            log.info("----->Start Skynet<-----");
            server = new ServerSocket(4567);
            while (true) {
                connetcion = server.accept();
                log.info("----->Someone connected to the Skynet<-----");
                output = new ObjectOutputStream(connetcion.getOutputStream());
                input = new ObjectInputStream(connetcion.getInputStream());
                log.info("----->Skynet is waiting for request<-----");
                try {
                    User us = (User) input.readObject();
                    log.info("----->Skynet received a request<-----");
                    serverManager(us);
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Exception: ", e);
                    log.info("----->The user is disconnected from the Skynet<-----");
                }
            }
        } catch (IOException ex) {
            log.log(Level.SEVERE, "Exception: ", ex);
        }
    }

    /**
     * The method send data on client.
     * @param obj the object to send
     */
    private static void sendDate(Object obj) {
        try {
            output.flush();
            output.writeObject(obj);
            log.info("----->Skynet sent the data<-----");
        } catch (Exception e) {
            log.log(Level.SEVERE, "Exception: ", e);
        }
    }

    private synchronized void serverManager(User us) {
        log.info("----->Skynet launched managed servers<-----");
        log.info(us.toString());
        int operation = us.getOperationNumber();
        log.info("----->Transaction number is " + operation + "<-----");

        switch (operation) {
            case 0:
                log.log(Level.INFO, "----->Skynet began to create user<-----");
                registrationUser(us);
                break;
            case 1:
                log.info("----->Skynet began to read user<-----");
                authorizationUser(us);
                break;
            case 2:
                log.info("----->Skynet began to update user<-----");
                updateUser(us);
                break;
            case 3:
                log.info("----->Skynet began to delete user<-----");
                deleteUser(us);
                break;
            case 4:
                log.info("----->Skynet began to refresh information about "
                        + "student university relationship<-----");
                refreshNM(us);
                break;
            case 5:
                log.info("----->Skynet began to delete university<-----");
                deleteUniversity(us);
                break;
            case 6:
                log.info("----->Skynet began to search knowledge exchenge offers<-----");
                knowExchangeSearch(us);
                break;
        }
    }

    private void registrationUser(User user) {
        log.info("----->Skynet trying to register a user<-----");
        UserDao dao = new UserDao();

        SKSuser sksuser = new SKSuser();
        sksuser.setId(0);
        sksuser.setName(user.getName());
        sksuser.setSurename(user.getSurname());
        sksuser.setMail(user.getMail());
        sksuser.setPassword(user.getPassword());
        sksuser.setCity(citySelector(user.getCity()));

        try {
            dao.addUser(sksuser);
            user.setRegistrationCompilte(Boolean.TRUE);
        } catch (Exception e) {
            user.setRegistrationCompilte(Boolean.FALSE);
            log.log(Level.SEVERE, "Exception: ", e);
        } finally {
            sendDate(user);
        }
    }

    private City citySelector(String city) {
        log.info("----->Skynet trying to select city for user<-----");
        City town = null;
        switch (city) {
            case "PRAHA":
                town = City.PRAHA;
                break;
            case "BRNO":
                town = City.BRNO;
                break;
            case "OLOMOUC":
                town = City.OLOMOUC;
                break;
            case "PLZEN":
                town = City.PLZEN;
                break;
            case "LIBIREC":
                town = City.LIBIREC;
                break;
            case "OSTRAVA":
                town = City.OSTRAVA;
                break;
        }
        return town;
    }

    private void authorizationUser(User user) {
        log.info("----->Skynet trying to authorize a user<-----");
        UserDao dao = new UserDao();
        try {
            SKSuser sksuser = dao.getUserByEmail(user.getMail());
            log.info("Password, which has registed user: " + user.getPassword()
                    + ". Entering password: " + sksuser.getPassword() + ". They"
                    + " are identical?");

            if (user.getPassword().equals(sksuser.getPassword())) {
                log.info("Yes, they are!");
                log.info("----->Skynet uploading user information<-----");
                user.setName(sksuser.getName());
                user.setSurname(sksuser.getSurename());
                user.setCity(sksuser.getCity().name());
                user.setCzu(Boolean.FALSE);
                user.setCvut(Boolean.FALSE);
                user.setUjop(Boolean.FALSE);
                user.setVse(Boolean.FALSE);
                user.setUk(Boolean.FALSE);
                user.setJipka(Boolean.FALSE);
                user.setAngDes(Boolean.FALSE);
                user.setNemDes(Boolean.FALSE);
                user.setCzDes(Boolean.FALSE);
                user.setLagDes(Boolean.FALSE);
                user.setMaDes(Boolean.FALSE);
                user.setWaDes(Boolean.FALSE);
                user.setAngOf(Boolean.FALSE);
                user.setNemOf(Boolean.FALSE);
                user.setCzOf(Boolean.FALSE);
                user.setLagOf(Boolean.FALSE);
                user.setMaOf(Boolean.FALSE);
                user.setWaOf(Boolean.FALSE);
                try {
                    log.info("----->Skynet trying to get information on user "
                            + "education<-----");
                    Set<University> universitys = sksuser.getUniversitys();
                    for (University university : universitys) {
                        int unId = university.getUniversity().getUnId();
                        if (unId == 0) {
                            user.setCzu(Boolean.TRUE);
                            log.info("User studies at CZU.");
                        }
                        if (unId == 1) {
                            user.setCvut(Boolean.TRUE);
                            log.info("User studies at CVUT.");
                        }
                        if (unId == 2) {
                            user.setUjop(Boolean.TRUE);
                            log.info("User studies at UJOP.");
                        }
                        if (unId == 3) {
                            user.setVse(Boolean.TRUE);
                            log.info("User studies at VSE.");
                        }
                        if (unId == 4) {
                            user.setUk(Boolean.TRUE);
                            log.info("User studies at UK.");
                        }
                        if (unId == 5) {
                            user.setJipka(Boolean.TRUE);
                            log.info("User studies at JIPKA.");
                        }
                    }
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Exception: ", e);
                }
                try {
                    log.info("----->Skynet trying to get information on user "
                            + "offer knowledge<-----");
                    Set<OfferKnowledge> offers = sksuser.getOffers();
                    for (OfferKnowledge offer : offers) {
                        int ofId = offer.getSubject().getSubjectId();
                        if (ofId == 0) {
                            user.setAngOf(Boolean.TRUE);
                            log.info("User offers ANG.");
                        }
                        if (ofId == 1) {
                            user.setNemOf(Boolean.TRUE);
                            log.info("User offers NEM.");
                        }
                        if (ofId == 2) {
                            user.setCzOf(Boolean.TRUE);
                            log.info("User offers CZ.");
                        }
                        if (ofId == 3) {
                            user.setLagOf(Boolean.TRUE);
                            log.info("User offers LAG.");
                        }
                        if (ofId == 4) {
                            user.setMaOf(Boolean.TRUE);
                            log.info("User offers MA.");
                        }
                        if (ofId == 5) {
                            user.setWaOf(Boolean.TRUE);
                            log.info("User offers WA.");
                        }
                    }
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Exception: ", e);
                }
                try {
                    log.info("----->Skynet trying to get information on user "
                            + "desire knowledge<-----");
                    Set<DesireKnowledge> desires = sksuser.getDesires();
                    for (DesireKnowledge desire : desires) {
                        int deId = desire.getSubject().getSubjectId();
                        if (deId == 0) {
                            user.setAngDes(Boolean.TRUE);
                            log.info("User desires ANG.");
                        }
                        if (deId == 1) {
                            user.setNemDes(Boolean.TRUE);
                            log.info("User desires NEM.");
                        }
                        if (deId == 2) {
                            user.setCzDes(Boolean.TRUE);
                            log.info("User desires CZ.");
                        }
                        if (deId == 3) {
                            user.setLagDes(Boolean.TRUE);
                            log.info("User desires LAG.");
                        }
                        if (deId == 4) {
                            user.setMaDes(Boolean.TRUE);
                            log.info("User desires MA.");
                        }
                        if (deId == 5) {
                            user.setWaDes(Boolean.TRUE);
                            log.info("User desires WA.");
                        }
                    }
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Exception: ", e);
                }
                user.setLoginComplite(Boolean.TRUE);
            } else {
                log.info("No, they are not!");
                user.setLoginComplite(Boolean.FALSE);
            }
        } catch (Exception e) {
            user.setLoginComplite(Boolean.FALSE);
            log.log(Level.SEVERE, "Exception: ", e);
        } finally {
            sendDate(user);
        }
    }

    private void updateUser(User user) {
        log.info("----->Skynet trying to update a user<-----");
        UserDao udao = new UserDao();
        SKSuser sksuserTry = udao.getUserByEmail(user.getMail());
        if (!(user.getMail().equals(user.getOldMail())) & !(sksuserTry == null)) {
            user.setUpdateComplite(Boolean.FALSE);
        } else {
            SKSuser sksuser = udao.getUserByEmail(user.getOldMail());
            sksuser.setName(user.getName());
            sksuser.setSurename(user.getSurname());
            sksuser.setMail(user.getMail());
            sksuser.setPassword(user.getPassword());
            sksuser.setCity(citySelector(user.getCity()));
            udao.updateUser(sksuser);
            updateUniversity(user, sksuser);
            updateDesire(user, sksuser);
            updateOffer(user, sksuser);
            user.setUpdateComplite(Boolean.TRUE);
        }
        sendDate(user);
    }

    private void deleteUser(User user) {
        UserDao udao = new UserDao();
        log.info("----->Skynet trying to delete a user<-----");
        SKSuser sksuser;
        try {
            udao.deleteUser(Integer.parseInt(user.getId()));
            refreshNM(user);
        } catch (Exception e) {
            log.log(Level.SEVERE, "Exception: ", e);
            try {
                sksuser = udao.getUserByEmail(user.getOldMail());
                user.setDelComplite(Boolean.TRUE);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "Exception: ", ex);
                user.setDelComplite(Boolean.FALSE);
            }
        }

        sendDate(user);
    }

    private void updateUniversity(User user, SKSuser sksuser) {
        log.info("----->Skynet trying to update a university<-----");
        UniversityDao udao = new UniversityDao();
        if (user.czu.booleanValue()) {
            University unTry = udao.getUniversityById(0);
            if (unTry == null) {
                University newUn = University.createUniversity(0, EnumUniversity.CZU);
                newUn.addStudent(sksuser);
                udao.addUniversity(newUn);
            } else if (!unTry.getStudents().contains(sksuser)) {
                unTry.addStudent(sksuser);
                udao.updateUniversity(unTry);
            }
        } else {
            University unTry = udao.getUniversityById(0);
            if (!(unTry == null)) {
                if (unTry.getStudents().contains(sksuser)) {
                    unTry.getStudents().remove(sksuser);
                    udao.updateUniversity(unTry);
                }
            }
        }

        if (user.cvut.booleanValue()) {
            University unTry = udao.getUniversityById(1);
            if (unTry == null) {
                University newUn = University.createUniversity(0, EnumUniversity.CVUT);
                newUn.addStudent(sksuser);
                udao.addUniversity(newUn);
            } else if (!unTry.getStudents().contains(sksuser)) {
                unTry.addStudent(sksuser);
                udao.updateUniversity(unTry);
            }
        } else {
            University unTry = udao.getUniversityById(1);
            if (!(unTry == null)) {
                if (unTry.getStudents().contains(sksuser)) {
                    unTry.getStudents().remove(sksuser);
                    udao.updateUniversity(unTry);
                }
            }
        }
        if (user.ujop.booleanValue()) {
            University unTry = udao.getUniversityById(2);
            if (unTry == null) {
                University newUn = University.createUniversity(0, EnumUniversity.UJOP);
                newUn.addStudent(sksuser);
                udao.addUniversity(newUn);
            } else if (!unTry.getStudents().contains(sksuser)) {
                unTry.addStudent(sksuser);
                udao.updateUniversity(unTry);
            }
        } else {
            University unTry = udao.getUniversityById(2);
            if (!(unTry == null)) {
                if (unTry.getStudents().contains(sksuser)) {
                    unTry.getStudents().remove(sksuser);
                    udao.updateUniversity(unTry);
                }
            }
        }
        if (user.vse.booleanValue()) {
            University unTry = udao.getUniversityById(3);
            if (unTry == null) {
                University newUn = University.createUniversity(0, EnumUniversity.VSE);
                newUn.addStudent(sksuser);
                udao.addUniversity(newUn);
            } else if (!unTry.getStudents().contains(sksuser)) {
                unTry.addStudent(sksuser);
                udao.updateUniversity(unTry);
            }
        } else {
            University unTry = udao.getUniversityById(3);
            if (!(unTry == null)) {
                if (unTry.getStudents().contains(sksuser)) {
                    unTry.getStudents().remove(sksuser);
                    udao.updateUniversity(unTry);
                }
            }
        }
        if (user.uk.booleanValue()) {
            University unTry = udao.getUniversityById(4);
            if (unTry == null) {
                University newUn = University.createUniversity(0, EnumUniversity.UK);
                newUn.addStudent(sksuser);
                udao.addUniversity(newUn);
            } else if (!unTry.getStudents().contains(sksuser)) {
                unTry.addStudent(sksuser);
                udao.updateUniversity(unTry);
            }
        } else {
            University unTry = udao.getUniversityById(4);
            if (!(unTry == null)) {
                if (unTry.getStudents().contains(sksuser)) {
                    unTry.getStudents().remove(sksuser);
                    udao.updateUniversity(unTry);
                }
            }
        }
        if (user.jipka.booleanValue()) {
            University unTry = udao.getUniversityById(5);
            if (unTry == null) {
                University newUn = University.createUniversity(0, EnumUniversity.JIPKA);
                newUn.addStudent(sksuser);
                udao.addUniversity(newUn);
            } else if (!unTry.getStudents().contains(sksuser)) {
                unTry.addStudent(sksuser);
                udao.updateUniversity(unTry);
            }
        } else {
            University unTry = udao.getUniversityById(5);
            if (!(unTry == null)) {
                if (unTry.getStudents().contains(sksuser)) {
                    unTry.getStudents().remove(sksuser);
                    udao.updateUniversity(unTry);
                }
            }
        }
    }

    private void updateDesire(User user, SKSuser sksuser) {
        log.info("----->Skynet trying to update a disare knowledge of "
                + user.getName() + "<-----");
        Set<DesireKnowledge> desires = sksuser.getDesires();
        DesireKnowledgeDao ddao = new DesireKnowledgeDao();
        if (user.angDes.booleanValue()) {
            int count = 0;
            for (DesireKnowledge desire : desires) {
                if (desire.getSubject().getSubjectId() == 0) {
                    count++;
                }
            }
            if (count == 0) {
                DesireKnowledge angDes = DesireKnowledge.createNewDesire(count,
                        Subject.ANG, sksuser);
                ddao.addDesire(angDes);
            }
        } else {
            for (DesireKnowledge desire : desires) {
                if (desire.getSubject().getSubjectId() == 0) {
                    ddao.deleteDesireKnowledge(desire.getId());
                }
            }
        }
        if (user.nemDes.booleanValue()) {
            int count = 0;
            for (DesireKnowledge desire : desires) {
                if (desire.getSubject().getSubjectId() == 1) {
                    count++;
                }
            }
            if (count == 0) {
                DesireKnowledge nemDes = DesireKnowledge.createNewDesire(count,
                        Subject.NEM, sksuser);
                ddao.addDesire(nemDes);
            }
        } else {
            for (DesireKnowledge desire : desires) {
                if (desire.getSubject().getSubjectId() == 1) {
                    ddao.deleteDesireKnowledge(desire.getId());
                }
            }
        }
        if (user.czDes.booleanValue()) {
            int count = 0;
            for (DesireKnowledge desire : desires) {
                if (desire.getSubject().getSubjectId() == 2) {
                    count++;
                }
            }
            if (count == 0) {
                DesireKnowledge czDes = DesireKnowledge.createNewDesire(count,
                        Subject.CZ, sksuser);
                ddao.addDesire(czDes);
            }
        } else {
            for (DesireKnowledge desire : desires) {
                if (desire.getSubject().getSubjectId() == 2) {
                    ddao.deleteDesireKnowledge(desire.getId());
                }
            }
        }
        if (user.lagDes.booleanValue()) {
            int count = 0;
            for (DesireKnowledge desire : desires) {
                if (desire.getSubject().getSubjectId() == 3) {
                    count++;
                }
            }
            if (count == 0) {
                DesireKnowledge lagDes = DesireKnowledge.createNewDesire(count,
                        Subject.LAG, sksuser);
                ddao.addDesire(lagDes);
            }
        } else {
            for (DesireKnowledge desire : desires) {
                if (desire.getSubject().getSubjectId() == 3) {
                    ddao.deleteDesireKnowledge(desire.getId());
                }
            }
        }
        if (user.maDes.booleanValue()) {
            int count = 0;
            for (DesireKnowledge desire : desires) {
                if (desire.getSubject().getSubjectId() == 4) {
                    count++;
                }
            }
            if (count == 0) {
                DesireKnowledge maDes = DesireKnowledge.createNewDesire(count,
                        Subject.MA, sksuser);
                ddao.addDesire(maDes);
            }
        } else {
            for (DesireKnowledge desire : desires) {
                if (desire.getSubject().getSubjectId() == 4) {
                    ddao.deleteDesireKnowledge(desire.getId());
                }
            }
        }
        if (user.waDes.booleanValue()) {
            int count = 0;
            for (DesireKnowledge desire : desires) {
                if (desire.getSubject().getSubjectId() == 5) {
                    count++;
                }
            }
            if (count == 0) {
                DesireKnowledge waDes = DesireKnowledge.createNewDesire(count,
                        Subject.WA, sksuser);
                ddao.addDesire(waDes);
            }
        } else {
            for (DesireKnowledge desire : desires) {
                if (desire.getSubject().getSubjectId() == 5) {
                    ddao.deleteDesireKnowledge(desire.getId());
                }
            }
        }
    }

    private void updateOffer(User user, SKSuser sksuser) {
        log.info("----->Skynet trying to update a offer  knowledge of "
                + user.getName() + "<-----");
        Set<OfferKnowledge> offers = sksuser.getOffers();
        OfferKnowledgeDao odao = new OfferKnowledgeDao();
        if (user.angOf.booleanValue()) {
            int count = 0;
            for (OfferKnowledge offer : offers) {
                if (offer.getSubject().getSubjectId() == 0) {
                    count++;
                }
            }
            if (count == 0) {
                OfferKnowledge angOf = OfferKnowledge.createNewOffer(count,
                        Subject.ANG, sksuser);
                odao.addOffer(angOf);
            }
        } else {
            for (OfferKnowledge offer : offers) {
                if (offer.getSubject().getSubjectId() == 0) {
                    odao.deleteOfferKnowledge(offer.getId());
                }
            }
        }
        if (user.nemOf.booleanValue()) {
            int count = 0;
            for (OfferKnowledge offer : offers) {
                if (offer.getSubject().getSubjectId() == 1) {
                    count++;
                }
            }
            if (count == 0) {
                OfferKnowledge nemOf = OfferKnowledge.createNewOffer(count,
                        Subject.NEM, sksuser);
                odao.addOffer(nemOf);
            }
        } else {

            for (OfferKnowledge offer : offers) {
                if (offer.getSubject().getSubjectId() == 1) {
                    odao.deleteOfferKnowledge(offer.getId());
                }
            }
        }
        if (user.czOf.booleanValue()) {
            int count = 0;
            for (OfferKnowledge offer : offers) {
                if (offer.getSubject().getSubjectId() == 2) {
                    count++;
                }
            }
            if (count == 0) {
                OfferKnowledge czOf = OfferKnowledge.createNewOffer(count,
                        Subject.CZ, sksuser);
                odao.addOffer(czOf);
            }
        } else {

            for (OfferKnowledge offer : offers) {
                if (offer.getSubject().getSubjectId() == 2) {
                    odao.deleteOfferKnowledge(offer.getId());
                }
            }
        }
        if (user.lagOf.booleanValue()) {
            int count = 0;
            for (OfferKnowledge offer : offers) {
                if (offer.getSubject().getSubjectId() == 3) {
                    count++;
                }
            }
            if (count == 0) {
                OfferKnowledge lagOf = OfferKnowledge.createNewOffer(count,
                        Subject.LAG, sksuser);
                odao.addOffer(lagOf);
            }
        } else {
            for (OfferKnowledge offer : offers) {
                if (offer.getSubject().getSubjectId() == 3) {
                    odao.deleteOfferKnowledge(offer.getId());
                }
            }
        }
        if (user.maOf.booleanValue()) {
            int count = 0;
            for (OfferKnowledge offer : offers) {
                if (offer.getSubject().getSubjectId() == 4) {
                    count++;
                }
            }
            if (count == 0) {
                OfferKnowledge maOf = OfferKnowledge.createNewOffer(count,
                        Subject.MA, sksuser);
                odao.addOffer(maOf);
            }
        } else {
            for (OfferKnowledge offer : offers) {
                if (offer.getSubject().getSubjectId() == 4) {
                    odao.deleteOfferKnowledge(offer.getId());
                }
            }
        }
        if (user.waOf.booleanValue()) {
            int count = 0;
            for (OfferKnowledge offer : offers) {
                if (offer.getSubject().getSubjectId() == 5) {
                    count++;
                }
            }
            if (count == 0) {
                OfferKnowledge waOf = OfferKnowledge.createNewOffer(count,
                        Subject.WA, sksuser);
                odao.addOffer(waOf);
            }
        } else {
            for (OfferKnowledge offer : offers) {
                if (offer.getSubject().getSubjectId() == 5) {
                    odao.deleteOfferKnowledge(offer.getId());
                }
            }
        }
    }

    private void refreshNM(User user) {
        log.info("----->Skynet trying to refresh data from user_university tabel<-----");
        int relation = 0;
        int count = 0;
        UniversityDao udao = new UniversityDao();
        List<University> allUniversity = udao.getAllUniversity();
        try {
            for (University university : allUniversity) {
                for (SKSuser sksuser : university.getStudents()) {
                    relation++;
                }
            }
            Object[][] obj = new Object[relation][4];
            for (University university : allUniversity) {
                Set<SKSuser> students = university.getStudents();
                if (!students.equals(null)) {
                    for (SKSuser student : students) {
                        obj[count][0] = university.getId();
                        obj[count][1] = university.getUniversity().getName();
                        obj[count][2] = student.getName() + " " + student.getSurename();
                        obj[count][3] = student.getId();
                        count++;
                    }
                }
            }
            user.setObj(obj);
            user.setRefreshNMComplite(Boolean.TRUE);
        } catch (Exception e) {
            log.log(Level.SEVERE, "Exception: ", e);
            user.setRefreshNMComplite(Boolean.FALSE);
        } finally {
            sendDate(user);
        }
    }

    private void deleteUniversity(User user) {
        log.info("----->Skynet trying to delete university<-----");
        UniversityDao udao = new UniversityDao();
        try {
            udao.deleteUniversity(Integer.parseInt(user.getId()));
            refreshNM(user);

        } catch (Exception e) {
            user.setRefreshNMComplite(Boolean.FALSE);
            sendDate(user);
        }
    }

    private void knowExchangeSearch(User user) {
        log.info("----->Skynet trying to <-----");
        try {
            UserDao udao = new UserDao();
            KnowledgeExchange ke = new KnowledgeExchange();
            SKSuser userByEmail = udao.getUserByEmail(user.getMail());
            user.setObj(ke.search(userByEmail));
            user.setRefreshKEComplite(Boolean.TRUE);
        } catch (Exception e) {
            log.log(Level.SEVERE, "Exception: ", e);
            user.setRefreshKEComplite(Boolean.FALSE);
        } finally {
            sendDate(user);
        }
    }
}
