/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gribanov.sksmaven.SearchKnowledgeExenge;

import gribanov.sksmaven.DAO.UserDao;
import gribanov.sksmaven.source.DesireKnowledge;
import gribanov.sksmaven.source.OfferKnowledge;
import gribanov.sksmaven.source.SKSuser;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Represents a candidates search system for exchanges of knowledge
 * A student can be enrolled in many university.
 * A student has to offer and require a lot of subject.
 * @author artem
 */
public class KnowledgeExchange {

    private static Logger log = Logger.getLogger(KnowledgeExchange.class.getName());

    private Set<SKSuser> selected1 = new HashSet<SKSuser>();
    private Set<SKSuser> selected2 = new HashSet<SKSuser>();
    int relation;
    int count;

    /**
    * Selects the most suitable candidates for the exchange of knowledge 
    * on the basis of the possibilities and requirements.
    * 
    * The method returns an array with a user's contact data.
    * 
    * @param aplicant the aplicant to set
    * @return the array
    */
    public Object[][] search(SKSuser aplicant) {
        log.log(Level.INFO, "--------------Hibernate + JavaDB + search");
        relation = 0;
        count = 0;
        UserDao us = new UserDao();
        List<SKSuser> uss = us.getAllUsers();
        uss.remove(aplicant);
        for (DesireKnowledge desire : aplicant.getDesires()) {
            log.log(Level.INFO, desire.getSubject().getName() + " - Search something similary");
            for (SKSuser user : uss) {
                for (OfferKnowledge ok : user.getOffers()) {
                    if (ok.getSubject().getSubjectId() == desire.getSubject().getSubjectId()) {
                        log.log(Level.INFO, "User, which has name " + user.getName() + " desire " + ok.getSubject().getName());
                        selected1.add(user);
                    } else {
                        log.log(Level.INFO, ok.getSubject().getName() + "Not equae");
                    }
                }
            }
        }

        for (OfferKnowledge offer : aplicant.getOffers()) {
            log.log(Level.INFO, aplicant.getName() + " can give " + offer.getSubject().getName());
            for (SKSuser user : selected1) {
                for (DesireKnowledge desire : user.getDesires()) {
                    if (desire.getSubject().getSubjectId() == offer.getSubject().getSubjectId()) {
                        log.log(Level.INFO, "User, which has name " + user.getName() + " want " + offer.getSubject());
                        selected2.add(user);
                    }
                }
            }
        }

        for (SKSuser student : selected2) {
                for (OfferKnowledge ofr : student.getOffers()) {
                    for (DesireKnowledge desire : aplicant.getDesires()) {
                        if (ofr.getSubject().equals(desire.getSubject())) {
                            relation++;
                        } 
                    }
                }
        }

        Object[][] obj = new Object[relation][5];
        try {
            for (SKSuser student : selected2) {
                for (OfferKnowledge ofr : student.getOffers()) {
                    for (DesireKnowledge desire : aplicant.getDesires()) {
                        if (ofr.getSubject().equals(desire.getSubject())) {
                            log.info(student.getName());
                            log.info(student.getSurename());
                            log.info(student.getMail());
                            log.info(ofr.getSubject().getName());
                            obj[count][0] = student.getName();
                            obj[count][1] = student.getSurename();
                            obj[count][2] = student.getMail();
                            obj[count][3] = student.getCity().getName();
                            obj[count][4] = ofr.getSubject().getName();
                            count++;
                        }      
                    }             
                }       
            }
            
        } catch (Exception e) {
            log.log(Level.SEVERE, "Exception: ", e);
        }
        return obj;
    }
}
